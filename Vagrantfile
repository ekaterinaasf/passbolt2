# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2"
VAGRANTFILE_API_VERSION = "2"

machines = [
    {
        :name => "test.dev",
        :box => "generic/centos7",
        :group => "test",
        :mem => "512",
        :cpu => "1",
        :network => [
            {
                :address => "192.168.40.10",
                :netmask => "255.255.255.0",
            },
        ],
    },
]

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    # disable guest additions update/install
    config.vbguest.auto_update = false
    config.vbguest.no_install = true

    machines.each do |opts|
        config.vm.define opts[:name] do |config|
            config.vm.box = opts[:box]
            config.vm.box_check_update = true
            config.vm.hostname = opts[:name]
            config.vm.provider "virtualbox" do |v|
            	v.customize ["modifyvm", :id, "--memory", opts[:mem]]
            	v.customize ["modifyvm", :id, "--cpus", opts[:cpu]]
            	v.customize ["modifyvm", :id, "--vrde", "on"]
            	v.customize ["modifyvm", :id, "--vrdeaddress", ""]
            	v.gui = false
            end
            unless opts[:network].nil?
                opts[:network].each do |network|
                    config.vm.network :private_network, ip: network[:address], netmask: network[:netmask]
                end
            end
            unless opts[:fw_port].nil?
                opts[:fw_port].each do |fw_port|
                    config.vm.network "forwarded_port", guest_ip: fw_port[:guest_ip], guest: fw_port[:guest], host: fw_port[:host]
                end
            end
        end
    end
end
